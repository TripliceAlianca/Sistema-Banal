## Implemente o seguinte sistema
1. Login e senha para acessar o sistema. (utilizar o padrão da prova 3 logins errados o usuário bloqueia).
2. Tipos de usuários (Login - num matricula, senha, e-mail, nome, endereço,  foto)
  1. Técnico administrativo em educação (TAE) - Setor, salario
  2. Aluno - ano primeira matricula, ativo
  3. Professor - área, salario, data ingresso, curriculo lattes (link)
3. Gerenciar as seguintes modalidades,
  1. Integrado (Ensino médio mais curso técnico)
        1. Particularidade turma fechada com um conjunto predefinido de disciplinas
  2. Superior/Subsequente (deve se identificar o nível)
4. Gerenciar os seguintes dados
    1. Disciplina - nome, ementa, objetivos, conteúdo programático, referencias, curso(s) (modelo na disciplina)
    2. Turma - Horário, professor, alunos, disciplina, provas, chamada
    3. Curso - turno, modalidade, alunos, duração, disciplinas, PPC (arquivo)
5. Algumas regras
    1. Quem pode criar turmas, cursos e disciplinas são os TAE
    2. Os professores podem trocar a bibliografia básica objetivos e cronograma mas não podem trocar a ementa.
    3. Os alunos poderão ver as notas, e se matricular em turmas e cursos caso haja vaga.


## Requisitos da disciplina

1. Implementar em JAVA
2.  Utilizar um banco de dados remoto
3.  Utilizar um JavaFX
4. Dividir o projeto em pacotes
5.  Utilizar o padrão MVC
6. Utilizar o padrão ActiveRecord
7. Gerar um Modelo ER nem que por engenharia reversa usando o ERMaster

## Regras

1. Trios auto organizáveis.
2. Link do gitlab e/ou github
3. REGRA FANTASMA 