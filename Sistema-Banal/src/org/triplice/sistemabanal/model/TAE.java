/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.triplice.sistemabanal.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.bigolin.Conexao;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 *
 * @author pietro
 */
public class TAE extends Usuario{

    private double salario;
    
    private String setor;
    
    public void criarDisciplina() {
        throw new NotImplementedException();
    }
    
    public void criarCurso() {
        throw new NotImplementedException();
    }
    
    public void criarTurma() {
        throw new NotImplementedException();
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public String getSetor() {
        return setor;
    }

    public void setSetor(String setor) {
        this.setor = setor;
    }
    
    @Override
    public boolean insert() {
        
        super.insert();
        
        String selectSQL = "INSERT INTO tae VALUES(?, ?, ?)";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        PreparedStatement ps;
        try {
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, getIdBanco());
            ps.setDouble(2, salario);         
            ps.setString(3, setor);
            
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return true;
    }
    
    @Override
    public boolean update() {
        
        super.update();
        
        String selectSQL = "UPDATE tae SET salario = ?, setor = ? WHERE id = ?";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        PreparedStatement ps;
        try {
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setDouble(1, salario);
            ps.setString(2, setor);
            ps.setInt(3, getIdBanco());
            
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return true;
    }
    
    @Override
    public boolean delete() {
         String selectSQL = "DELETE FROM tae WHERE id = ?";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        PreparedStatement ps;
        try {

            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, getIdBanco());

            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        super.delete();
        
        return true;
    }
    
    @Override 
        public void load() {
        
        super.load();
        
        String selectSQL = "SELECT * FROM tae WHERE id = ?";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        PreparedStatement ps;
        try {
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, getIdBanco());
            
            ResultSet rs = ps.executeQuery();
            
            if(rs.next()) {
                setSetor(rs.getString("setor"));
                setSalario(rs.getDouble("salario"));
            }
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
}
