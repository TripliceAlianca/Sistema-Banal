/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.triplice.sistemabanal.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import org.bigolin.Conexao;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 *
 * @author pietro
 */
public class Aluno extends Usuario {

    private int anoMatricula;

    private boolean status;

    private ArrayList<Turma> turmas;

    private Curso curso;

    private String nivel;

    public String verNotas() {
        throw new NotImplementedException();
    }

    public void matricularCurso(Curso c) {
        throw new NotImplementedException();
    }

    @Override
    public boolean insert() {

        super.insert();

        String selectSQL = "INSERT INTO aluno(id,anomatricula,status,nivel) VALUES(?, ?, ?, ?)";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        PreparedStatement ps;
        try {
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, getIdBanco());
            ps.setInt(2, anoMatricula);
            ps.setString(3, status ? "1" : "0");
            ps.setString(4, nivel);

            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return true;
    }

    @Override
    public boolean update() {

        super.update();

        String selectSQL = "UPDATE aluno SET anomatricula = ?, status = ? WHERE id = ?";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        PreparedStatement ps;
        try {
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, anoMatricula);
            ps.setString(2, status ? "0" : "1");
            ps.setInt(3, getIdBanco());

            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return true;
    }

    @Override
    public boolean delete() {
        String selectSQL = "DELETE FROM aluno WHERE id = ?";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        PreparedStatement ps;
        try {

            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, getIdBanco());

            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        super.delete();

        return true;
    }

    @Override
    public void load() {

        super.load();

        String selectSQL = "SELECT * FROM aluno WHERE id = ?";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        PreparedStatement ps;
        try {
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, getIdBanco());

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int getAnoMatricula() {
        return anoMatricula;
    }

    public void setAnoMatricula(int anoMatricula) {
        this.anoMatricula = anoMatricula;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public ArrayList<Turma> getTurmas() {
        return turmas;
    }

    public void setTurmas(ArrayList<Turma> turmas) {
        this.turmas = turmas;
    }

    public Curso getCurso() {
        return curso;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }
}
