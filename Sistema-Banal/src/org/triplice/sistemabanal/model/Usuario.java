/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.triplice.sistemabanal.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.bigolin.ActiveRecord;
import org.bigolin.Conexao;

/**
 *
 * @author pietro
 */
public class Usuario implements ActiveRecord {

    private String login, senha, email, endereco, foto, nome;

    private int idBanco, tentativas;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public int getTentativas() {
        return tentativas;
    }

    public void setTentativas(int tentativas) {
        this.tentativas = tentativas;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public int getIdBanco() {
        return idBanco;
    }

    public void setIdBanco(int idBanco) {
        this.idBanco = idBanco;
    }

    @Override
    public boolean insert() {
        String selectSQL = "INSERT INTO usuario(id,login,senha,email,endereco,foto, tentativas,nome) VALUES(SEQ_USUARIO.NEXTVAL, ?, ?, ? ,?, ?,?)";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        PreparedStatement ps;
        try {
            ps = dbConnection.prepareStatement(selectSQL);
            
            ps.setString(1, login);
            ps.setString(2, senha);
            ps.setString(3, email);
            ps.setString(4, endereco);
            ps.setString(5, foto);
            ps.setInt(6, tentativas);
            ps.setString(7, nome);
            ps.executeUpdate();

            ps = dbConnection.prepareStatement("SELECT SEQ_USUARIO.CURRVAL FROM DUAL");
            ResultSet rs = ps.executeQuery();
            rs.next();
            idBanco = rs.getInt("CURRVAL");

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return true;
    }

    @Override
    public boolean delete() {
        String selectSQL = "DELETE FROM usuario WHERE id = ?";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        PreparedStatement ps;
        try {

            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, idBanco);

            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return true;
    }

    @Override
    public boolean update() {
        
        if (idBanco < 0) {
            return insert();
        }

        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;

        try {

            ps = dbConnection.prepareStatement("UPDATE usuario SET login=?, senha=?, email=?, endereco=?, foto=?, nome=? WHERE id = ?, tentativas=?");
            ps.setString(1, login);
            ps.setString(2, senha);
            ps.setString(3, email);
            ps.setString(4, endereco);
            ps.setString(5, foto);
            ps.setString(6, nome);
            ps.setInt(7, idBanco);
            ps.setInt(8, tentativas);

            ps.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            c.desconecta();
        }

        return true;
    }

    @Override
    public void load() {
        String selectSQL = "SELECT * FROM usuario WHERE id = ?";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        PreparedStatement ps;
        try {
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, this.idBanco);

            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                setLogin(rs.getString("login"));
                setSenha(rs.getString("senha"));
                setEmail(rs.getString("email"));
                setFoto(rs.getString("foto"));
                setEndereco(rs.getString("endereco"));
                setTentativas(rs.getInt("tentativas"));
                setNome(rs.getString("nome"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void load(int id) {
        setIdBanco(id);
        load();
    }
}
