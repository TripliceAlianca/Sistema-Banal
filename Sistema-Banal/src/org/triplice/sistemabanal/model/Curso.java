/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.triplice.sistemabanal.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import org.bigolin.ActiveRecord;
import org.bigolin.Conexao;

/**
 *
 * @author pietro
 */
public class Curso implements ActiveRecord{
    
    private String turno, modalidade, ppc;
    
    private int duracao, idBanco = -1;
    
    private ArrayList<Disciplina> disciplinas = new ArrayList<>();
    private ArrayList<Turma> turma = new ArrayList<>();
    
    public static ArrayList<Curso> loadAll() {
        
        ArrayList<Curso> res = new ArrayList<>();
        
        String selectSQL = "SELECT id FROM curso";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        PreparedStatement ps;
        try {
            ps = dbConnection.prepareStatement(selectSQL);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                
                Curso cu = new Curso();
                
                cu.load(rs.getInt("id"));
                
                res.add(cu);
            }
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return res;
        
    }

    @Override
    public boolean insert() {
        
        String selectSQL = "INSERT INTO curso VALUES(SEQ_CURSO.NEXTVAL, ?, ?, ?, ?)";
        String disciplinaSQL = "INSERT INTO disciplina_curso VALUES (?, ?)";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        PreparedStatement ps;
        try {
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setString(1, turno);
            ps.setString(2, modalidade);
            ps.setInt(3, duracao);
            ps.setString(4, ppc);
            
            ps.executeUpdate();
            
            for(Turma t : turma) {
                t.setCurso(this);
                t.insert();
            }
            
            for(Disciplina d : disciplinas) {
                d.insert();
                
                ps = dbConnection.prepareStatement(disciplinaSQL);
            
                ps.setInt(1, idBanco);
                ps.setInt(2, d.getId());
                
                ps.execute();
                ps.close();
            }

            ps = dbConnection.prepareStatement("select seq_curso.currval from dual");
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                idBanco = rs.getInt("CURRVAL");
            }
            
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            c.desconecta();
        }

        return true;
    }

    @Override
    public boolean delete() {
        String selectSQL = "DELETE FROM curso WHERE id = ?";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        PreparedStatement ps;
        try {

            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, idBanco);

            ps.executeUpdate();
            
            for(Turma t : turma) {
                t.delete();
            }
            
            
            for(Disciplina d : disciplinas) {
                d.delete();
            }
            
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return true;
    }
    
    @Override
    public boolean update() {
        if (idBanco < 0) {
            return insert();
        }

        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;

        try {

            ps = dbConnection.prepareStatement("UPDATE curso SET turno=?, modalidade=?, duracao=?, ppc=? WHERE id = ?");
            ps.setString(1, turno);
            ps.setString(2, modalidade);
            ps.setInt(3, duracao);
            ps.setString(4, ppc);
            ps.setInt(5, idBanco);
            
            
            ps.executeUpdate();
            
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            c.desconecta();
        }

        return true;
    }

    @Override
    public void load() {
        String selectSQL = "SELECT * FROM curso WHERE id = ?";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        PreparedStatement ps;
        try {
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, this.idBanco);

            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                setTurno(rs.getString("turno"));
                setModalidade(rs.getString("modalidade"));
                setDuracao(rs.getInt("duracao"));    
                setPpc(rs.getString("ppc"));
                
                loadTurmas();
                loadDisciplinas();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    private void loadTurmas() {
         String selectSQL = "SELECT * FROM turma WHERE id_curso = ?";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        PreparedStatement ps;
        try {
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, this.idBanco);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Turma t = new Turma();
                t.load(rs.getInt("id"));
                
                turma.add(t);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    private void loadDisciplinas() {
        String selectSQL = "SELECT * FROM disciplina_curso WHERE id_curso = ?";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        PreparedStatement ps;
        try {
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, this.idBanco);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Disciplina d = new Disciplina();
                d.load(rs.getInt("id_disciplina"));
                
                disciplinas.add(d);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String getTurno() {
        return turno;
    }

    public void setTurno(String turno) {
        this.turno = turno;
    }

    public String getModalidade() {
        return modalidade;
    }

    public void setModalidade(String modalidade) {
        this.modalidade = modalidade;
    }

    public String getPpc() {
        return ppc;
    }

    public void setPpc(String ppc) {
        this.ppc = ppc;
    }

    public int getDuracao() {
        return duracao;
    }

    public void setDuracao(int duracao) {
        this.duracao = duracao;
    }

    public int getIdBanco() {
        
        if(idBanco < 0)
            insert();
        
        return idBanco;
    }

    public void setIdBanco(int idBanco) {
        this.idBanco = idBanco;
    }

    public ArrayList<Disciplina> getDisciplinas() {
        return disciplinas;
    }

    public void setDisciplinas(ArrayList<Disciplina> disciplinas) {
        this.disciplinas = disciplinas;
    }

    public ArrayList<Turma> getTurma() {
        return turma;
    }

    public void setTurma(ArrayList<Turma> turma) {
        this.turma = turma;
    }

    @Override
    public void load(int id) {
        setIdBanco(id);
        load();
    }

    
}
