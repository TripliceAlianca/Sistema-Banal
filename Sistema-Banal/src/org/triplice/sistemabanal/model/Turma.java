/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.triplice.sistemabanal.model;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import org.bigolin.ActiveRecord;
import org.bigolin.Conexao;

/**
 *
 * @author pietro
 */
public class Turma implements ActiveRecord {

    private ArrayList<Horario> provas = new ArrayList<>(), horario = new ArrayList<>();

    public ArrayList<Horario> getHorario() {
        return horario;
    }

    public void setHorario(ArrayList<Horario> horario) {
        this.horario = horario;
    }

    private ArrayList<Professor> professores = new ArrayList<>();

    private int idTurma = -1;

    private ArrayList<Disciplina> disciplinas = new ArrayList<>();

    private Curso curso;

    public ArrayList<Horario> getProvas() {
        return provas;
    }

    public void setProvas(ArrayList<Horario> provas) {
        this.provas = provas;
    }

    public ArrayList<Professor> getProfessores() {
        return professores;
    }

    public void setProfessores(ArrayList<Professor> professores) {
        this.professores = professores;
    }

    public int getIdTurma() {
        return idTurma;
    }

    public void setIdTurma(int idTurma) {
        this.idTurma = idTurma;
    }

    public ArrayList<Disciplina> getDisciplinas() {
        return disciplinas;
    }

    public void setDisciplinas(ArrayList<Disciplina> disciplinas) {
        this.disciplinas = disciplinas;
    }

    public Curso getCurso() {
        return curso;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }

    @Override
    public boolean insert() {
        String selectSQL = "INSERT INTO turma VALUES(SEQ_TURMA.NEXTVAL, ?)";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        PreparedStatement ps;
        try {
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, curso.getIdBanco());
            ps.executeUpdate();

            ps = dbConnection.prepareStatement("SELECT SEQ_TURMA.CURRVAL FROM DUAL");
            ResultSet rs = ps.executeQuery();
            rs.next();
            idTurma = rs.getInt("CURRVAL");
            
            for (Horario h : provas) {
                // Garantir que disciplina esteja no banco
                h.getDisciplina().insert();

                ps = dbConnection.prepareStatement("INSERT INTO turma_disc_prova VALUES(?, ?, ?, ?)");
                ps.setInt(1, idTurma);
                ps.setInt(2, h.getDisciplina().getId());

                Date inicio = new Date(h.getInicio().getTimeInMillis());
                ps.setDate(3, inicio);

                Date fim = new Date(h.getFim().getTimeInMillis());
                ps.setDate(4, fim);
                
                ps.executeUpdate();
                ps.close();
            }

            for (Professor p : professores) {
                p.insert();

                ps = dbConnection.prepareStatement("INSERT INTO turma_professor VALUES(?, ?)");
                ps.setInt(1, p.getIdBanco());
                ps.setInt(2, idTurma);
                ps.executeUpdate();
                ps.close();
            }
            for (Disciplina d : disciplinas) {
                d.insert();

                ps = dbConnection.prepareStatement("INSERT INTO disciplina_turma VALUES(?, ?)");
                ps.setInt(1, idTurma);
                ps.setInt(2, d.getId());

                ps.executeUpdate();
                ps.close();
            }

            for (Horario h : horario) {
                h.getDisciplina().insert();

                ps = dbConnection.prepareStatement("INSERT INTO turma_disc_hors VALUES(?, ?, ?, ?)");
                ps.setInt(1, idTurma);
                ps.setInt(2, h.getDisciplina().getId());
                
                Date inicio = new Date(h.getInicio().getTimeInMillis());
                ps.setDate(3, inicio);

                Date fim = new Date(h.getFim().getTimeInMillis());
                ps.setDate(4, fim);
               
                ps.executeUpdate();
                ps.close();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public boolean delete() {
        String selectSQL = "DELETE FROM turma WHERE id = ?";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        PreparedStatement ps;
        try {
            ps = dbConnection.prepareStatement("DELETE FROM turma_disc_prova WHERE id_turma = ?");
            ps.setInt(1, idTurma);
            ps.executeUpdate();

            ps = dbConnection.prepareStatement("DELETE FROM turma_disc_hors WHERE id_turma = ?");
            ps.setInt(1, idTurma);
            ps.executeUpdate();

            ps = dbConnection.prepareStatement("DELETE FROM turma_professor WHERE id_turma = ?");
            ps.setInt(1, idTurma);
            ps.executeUpdate();

            ps = dbConnection.prepareStatement("DELETE FROM disciplina_turma WHERE id_turma = ?");
            ps.setInt(1, idTurma);
            ps.executeUpdate();

            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, idTurma);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return true;
    }

    @Override
    public boolean update() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        PreparedStatement ps;
        try {
            // Libera espaço pros novos
            ps = dbConnection.prepareStatement("DELETE FROM turma_disc_prova WHERE id_turma = ?");
            ps.setInt(1, idTurma);
            ps.executeUpdate();
            for (Horario h : provas) {
                // Garantir que disciplina esteja no banco
                h.getDisciplina().insert();

                // Bota os novos
                ps = dbConnection.prepareStatement("INSERT INTO turma_disc_prova (?, ?, ?, ?)");
                ps.setInt(1, idTurma);
                ps.setInt(2, h.getDisciplina().getId());

                Date inicio = new Date(h.getInicio().getTimeInMillis());
                ps.setDate(3, inicio);

                Date fim = new Date(h.getFim().getTimeInMillis());
                ps.setDate(4, fim);
                ps.executeUpdate();
            }

            // Libera espaço pros novos
            ps = dbConnection.prepareStatement("DELETE FROM turma_professor WHERE id_turma = ?");
            ps.setInt(1, idTurma);
            ps.executeUpdate();
            for (Professor p : professores) {
                p.insert();

                ps = dbConnection.prepareStatement("INSERT INTO turma_professor VALUES(?, ?)");
                ps.setInt(1, p.getIdBanco());
                ps.setInt(2, idTurma);
                ps.executeUpdate();
                ps.close();
            }

            // Libera espaço pros novos
            ps = dbConnection.prepareStatement("DELETE FROM disciplina_turma WHERE id_turma = ?");
            ps.setInt(1, idTurma);
            ps.executeUpdate();
            for (Disciplina d : disciplinas) {
                d.insert();

                ps = dbConnection.prepareStatement("INSERT INTO disciplina_turma VALUES(?, ?)");
                ps.setInt(1, idTurma);
                ps.setInt(2, d.getId());

                ps.executeUpdate();
                ps.close();
            }

            ps = dbConnection.prepareStatement("DELETE FROM turma_disc_hors WHERE id_turma = ?");
            ps.setInt(1, idTurma);
            ps.executeUpdate();
            for (Horario h : horario) {
                h.getDisciplina().insert();

                ps = dbConnection.prepareStatement("INSERT INTO turma_disc_hors (?, ?, ?, ?)");
                ps.setInt(1, idTurma);
                ps.setInt(2, h.getDisciplina().getId());

                Date inicio = new Date(h.getInicio().getTimeInMillis());
                ps.setDate(3, inicio);

                Date fim = new Date(h.getFim().getTimeInMillis());
                ps.setDate(4, fim);
                
                ps.executeUpdate();
                ps.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public void load() {
        String selectSQL = "SELECT * FROM turma WHERE id = ?";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        PreparedStatement ps;
        try {
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, this.idTurma);

            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Curso curso = new Curso();
                curso.load(rs.getInt("id_curso"));
                setCurso(curso);

                loadProvas(dbConnection);
                loadHorario(dbConnection);
                loadProfessores(dbConnection);
                loadDisciplinas(dbConnection);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            c.desconecta();
        }
    }

    private void loadProvas(Connection c) {
        try {
            PreparedStatement ps = c.prepareStatement("SELECT * FROM turma_disc_prova WHERE id_turma = ?");
            ps.setInt(1, idTurma);

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                // Hoje em coisas que só o Java faz pra ti:
                // Não dá pra dar new Calendar(), então a
                // gente faz essa coisa aí
                Calendar inicio = Calendar.getInstance(),
                        fim = Calendar.getInstance();

                Disciplina disc = new Disciplina();
                disc.load(rs.getInt("id_disciplina"));

                // Graças a Deus existe getDate();
                inicio.setTime(rs.getDate("inicio"));
                fim.setTime(rs.getDate("fim"));

                // "Empacota" todos os datos em uma
                // "caixa" bonitinha que chamamos de
                // horário
                Horario h = new Horario();
                h.setDisciplina(disc);
                h.setInicio(inicio);
                h.setFim(fim);

                // Vai botando as provas cara
                provas.add(h);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void loadHorario(Connection c) {
        try {
            PreparedStatement ps = c.prepareStatement("SELECT * FROM turma_disc_hors WHERE id_turma = ?");
            ps.setInt(1, idTurma);

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                // Hoje em coisas que só o Java faz pra ti:
                // Não dá pra dar new Calendar(), então a
                // gente faz essa coisa aí
                Calendar inicio = Calendar.getInstance(),
                        fim = Calendar.getInstance();

                Disciplina disc = new Disciplina();
                disc.load(rs.getInt("id_disciplina"));

                // Graças a Deus existe getDate();
                inicio.setTime(rs.getDate("inicio"));
                fim.setTime(rs.getDate("fim"));

                // "Empacota" todos os datos em uma
                // "caixa" bonitinha que chamamos de
                // horário
                Horario h = new Horario();
                h.setDisciplina(disc);
                h.setInicio(inicio);
                h.setFim(fim);

                // Vai botando os horários cara
                horario.add(h);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void loadProfessores(Connection c) {
        try {
            PreparedStatement ps = c.prepareStatement("SELECT * FROM turma_professor WHERE id_turma = ?");
            ps.setInt(1, idTurma);

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Professor p = new Professor();

                p.load(rs.getInt("id_professor"));

                professores.add(p);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void loadDisciplinas(Connection c) {
        try {
            PreparedStatement ps = c.prepareStatement("SELECT * FROM disciplina_turma WHERE id_turma = ?");
            ps.setInt(1, idTurma);

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Disciplina d = new Disciplina();

                d.load(rs.getInt("id_disciplina"));

                disciplinas.add(d);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void load(int id) {
        idTurma = id;
        load();
    }
}
