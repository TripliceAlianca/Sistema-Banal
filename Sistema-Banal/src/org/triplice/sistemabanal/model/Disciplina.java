/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.triplice.sistemabanal.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.bigolin.ActiveRecord;
import org.bigolin.Conexao;

/**
 *
 * @author pietro
 */
public class Disciplina implements ActiveRecord{
    private String nome, ementa, objetivos, cronograma, referencias;
    
    private int id;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmenta() {
        return ementa;
    }

    public void setEmenta(String ementa) {
        this.ementa = ementa;
    }

    public String getObjetivos() {
        return objetivos;
    }

    public void setObjetivos(String objetivos) {
        this.objetivos = objetivos;
    }

    public String getCronograma() {
        return cronograma;
    }

    public void setCronograma(String cronograma) {
        this.cronograma = cronograma;
    }

    public String getReferencias() {
        return referencias;
    }

    public void setReferencias(String referencias) {
        this.referencias = referencias;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    @Override
    public boolean insert() {
        String selectSQL = "INSERT INTO disciplina VALUES(SEQ_disciplina.NEXTVAL, ?, ?, ? ,?, ?)";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        PreparedStatement ps;
        try {
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setString(1, nome);
            ps.setString(2, ementa);
            ps.setString(3, objetivos);
            ps.setString(4, cronograma);            
            ps.setString(5, referencias);
            ps.executeUpdate();

            ps = dbConnection.prepareStatement("SELECT SEQ_disciplina.CURRVAL FROM DUAL");
            ResultSet rs = ps.executeQuery();
            rs.next();
            id = rs.getInt("CURRVAL");

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return true;
    }

    @Override
    public boolean delete() {
        String deleteSQL = "DELETE FROM disciplina WHERE id = ?";
        String deleteSQL1 = "DELETE FROM disciplina_curso WHERE id_disciplina = ?";
        String deleteSQL2 = "DELETE FROM disciplina_turma WHERE id_disciplina = ?";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        PreparedStatement ps;
        try {
               
            ps = dbConnection.prepareStatement(deleteSQL2);
            ps.setInt(1, id);            
            ps.executeUpdate();
            
            ps = dbConnection.prepareStatement(deleteSQL1);
            ps.setInt(1, id);            
            ps.executeUpdate();
            
            ps = dbConnection.prepareStatement(deleteSQL);
            ps.setInt(1, id);            
            ps.executeUpdate();
            
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return true;
    }

    @Override
    public boolean update() {
        
        if (id < 0) {
            return insert();
        }

        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;

        try {

            ps = dbConnection.prepareStatement("UPDATE disciplina SET nome=?, ementa=?, objetivos=?, cronograma=?, referencias=? WHERE id = ?");
            ps.setString(1, nome);
            ps.setString(2, ementa);
            ps.setString(3, objetivos);
            ps.setString(4, cronograma);
            ps.setString(5, referencias);
            ps.setInt(6, id);
            ps.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            c.desconecta();
        }

        return true;
    }

    @Override
    public void load() {
        String selectSQL = "SELECT * FROM disciplina WHERE id = ?";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        PreparedStatement ps;
        try {
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, this.id);

            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                setNome(rs.getString("nome"));
                setEmenta(rs.getString("ementa"));
                setObjetivos(rs.getString("objetivos"));
                setCronograma(rs.getString("cronograma"));
                setReferencias(rs.getString("referencias"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void load(int id) {
        load();
    }
}
