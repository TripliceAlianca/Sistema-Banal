/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.triplice.sistemabanal.model;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import org.bigolin.Conexao;

/**
 *
 * @author pietro
 */
public class Professor extends Usuario {
    
    private String area, linkLattes;
    
    private double salario;
    
    private Calendar dataIngresso;
    
    public Professor() {
        dataIngresso = Calendar.getInstance();
    }
    
    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getLinkLattes() {
        return linkLattes;
    }

    public void setLinkLattes(String linkLattes) {
        this.linkLattes = linkLattes;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public Calendar getDataIngresso() {
        return dataIngresso;
    }

    public void setDataIngresso(Calendar dataIngresso) {
        this.dataIngresso = dataIngresso;
    }
    
    
    
    @Override
    public boolean insert() {
        
        super.insert();
        
        String selectSQL = "INSERT INTO professor VALUES(?, ?, ?, ?, ?)";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        PreparedStatement ps;
        try {
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, getIdBanco());
            ps.setString(2, area);
            ps.setDouble(3, salario);
            ps.setDate(4, new java.sql.Date(dataIngresso.getTimeInMillis()));
            
            ps.setString(5, linkLattes);
            
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return true;
    }
    
    @Override
    public boolean update() {
        
        super.update();
        
        String selectSQL = "UPDATE professor SET area= ?, salario = ?, data_ingresso = ?, link_lattes = ? WHERE id = ?";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        PreparedStatement ps;
        try {
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setString(1, area);
            ps.setDouble(2, salario);
            ps.setDate(3, null, dataIngresso);
            ps.setString(4, linkLattes);
            ps.setInt(5, getIdBanco());
            
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return true;
    }
    
    @Override
    public boolean delete() {
         String selectSQL = "DELETE FROM professor WHERE id = ?";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        PreparedStatement ps;
        try {

            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, getIdBanco());

            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        super.delete();
        
        return true;
    }
    
    @Override 
        public void load() {
        
        super.load();
        
        String selectSQL = "SELECT * FROM professor WHERE id = ?";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        PreparedStatement ps;
        try {
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, getIdBanco());
            
            ResultSet rs = ps.executeQuery();
            
            if(rs.next()) {
                String sDia, sMes, sAno;
                int dia, mes, ano;
                setArea(rs.getString("area"));
                setLinkLattes(rs.getString("link_lattes"));
                setSalario(rs.getDouble("salario"));
                
                Date d = rs.getDate("data_ingresso");
                if (dataIngresso == null || d == null)
                    System.out.println("Please fucking kill me");
                else
                    dataIngresso.setTime(d);
            }
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}