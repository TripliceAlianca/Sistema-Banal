/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.triplice.sistemabanal.control;

import java.io.IOException;
import java.net.URL;
import java.util.Calendar;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import org.triplice.sistemabanal.model.Professor;

/**
 *
 * @author User
 */
public class ProfController extends Controller{
    
    Professor prof;

    @FXML
    Label nome;
    @FXML
    Label email;
    @FXML
    Label endereco;
    @FXML
    Label foto;
    @FXML
    Label lattes;
    @FXML
    Label usuario;
    @FXML
    Label area;
    @FXML
    Label salario;
    @FXML
    Label ingresso;
    
    public void setProf(Professor prof) {
        
        this.prof = prof;
        
        nome.setText(prof.getLogin());
        email.setText(prof.getEmail());
        endereco.setText(prof.getEndereco());
        // foto.setText(prof.getFoto());
        lattes.setText(prof.getLinkLattes());
        usuario.setText(prof.getLogin());
        area.setText(prof.getArea());
        salario.setText(Double.toString(prof.getSalario()));
        
        Calendar c = prof.getDataIngresso();
        String data = c.get(Calendar.DATE) + "/" + c.get(Calendar.MONTH) + "/" + c.get(Calendar.YEAR);
        ingresso.setText(data);
    }

    public Professor getProf() {
        return prof;
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {   
       
    }
    
    @FXML
    private void alterarObjetivos() {
        this.stage.setScene(getTelaObjetivos());
    }
    
    @FXML
    private void alterarCronograma() {
        this.stage.setScene(getTelaCrono());
    }
    
    private Scene getTelaObjetivos() {
    
        Scene scene = null;
        try {
            // Loader
            FXMLLoader loader = new FXMLLoader(getClass().getResource("ProfObjetivos.fxml"));
            AnchorPane rootLayout = (AnchorPane) loader.load();
            scene = new Scene(rootLayout);
        } catch (IOException e) {
            System.out.println("oi");
            e.printStackTrace();
        }
        return scene;
    }
    
    private Scene getTelaCrono() {
    
        Scene scene = null;
        try {
            // Loader
            FXMLLoader loader = new FXMLLoader(getClass().getResource("ProfCronograma.fxml"));
            AnchorPane rootLayout = (AnchorPane) loader.load();
            scene = new Scene(rootLayout);
        } catch (IOException e) {
            System.out.println("oi");
            e.printStackTrace();
        }
        return scene;
    }

}
