/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.triplice.sistemabanal.control;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import org.triplice.sistemabanal.model.Aluno;

/**
 *
 * @author User
 */
public class CadAlunoController extends Controller{
    
    @FXML 
    TextField tfNome;
    @FXML
    TextField tfEmail;
    @FXML
    TextField tfEndereco;
    @FXML
    TextField tfFoto;
    @FXML
    TextField tfAnoMat;
    @FXML
    TextField tfMat;
    @FXML
    TextField tfSenha;
    
    @FXML
    CheckBox chbStatus;
    
    @FXML
    ChoiceBox<String> cbNivel;
    
    @FXML
    Button btCadastrar;
    
    @FXML
    public void cadastrar(){
        Aluno a = new Aluno();
        a.setNome(tfNome.getText());
        a.setEmail(tfEmail.getText());
        a.setEndereco(tfEndereco.getText());
        a.setFoto(tfFoto.getText());
        a.setLogin(tfMat.getText());
        a.setSenha(tfSenha.getText()); 
        a.setAnoMatricula(Integer.parseInt((tfAnoMat.getText())));
        a.setNivel(cbNivel.getSelectionModel().getSelectedItem());
        if(chbStatus.isSelected()){
            a.setStatus(true);
        }
        else{
            a.setStatus(false);
        }
        
        a.insert();  
        
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {   
       cbNivel.setItems(FXCollections.observableArrayList("Integrado","Superior","EJA"));
    }
}
