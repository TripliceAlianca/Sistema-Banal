package org.triplice.sistemabanal.control;

import javafx.fxml.Initializable;
import javafx.stage.Stage;

public abstract class Controller implements Initializable{
    protected Stage stage;
    
    public void setStage(Stage st){
        this.stage = st;
    }
    
}