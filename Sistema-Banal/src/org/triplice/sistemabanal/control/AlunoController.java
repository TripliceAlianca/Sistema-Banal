package org.triplice.sistemabanal.control;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import org.triplice.sistemabanal.model.Aluno;

/**
 *
 * @author User
 */
public class AlunoController extends Controller{
    
    Aluno aluno;

    @FXML
    Label nome;
    @FXML
    Label email;
    @FXML
    Label endereco;
    @FXML
    Label foto;
    @FXML
    Label matricula;
    @FXML
    Label status;
    @FXML
    Label anom;
    @FXML
    Label nivel; 
    
    public Aluno getAluno() {
        return aluno;
    }

    public void setAluno(Aluno aluno) {
        this.aluno = aluno;
        
        nome.setText(aluno.getLogin());
        email.setText(aluno.getEmail());
        endereco.setText(aluno.getEndereco());
        // foto.setText(aluno.getFoto());
        matricula.setText(aluno.getLogin());
        status.setText(aluno.isStatus() ? "Ativo" : "Não Ativo");
        anom.setText(Integer.toString(aluno.getAnoMatricula()));
        nivel.setText(aluno.getNivel());
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {   
       
    }
    
    @FXML
    private void verNotas() {
        this.stage.setScene(getTelaNotas());
    }
    
    @FXML
    private void entarTurma() {
        this.stage.setScene(getTelaEntrarTurma());
    }
    
    @FXML
    private void entrarCurso() {
        this.stage.setScene(getTelaEntrarCurso());
    }
    
    private Scene getTelaNotas() {

        Scene scene = null;
        try {
            // Loader
            FXMLLoader loader = new FXMLLoader(getClass().getResource("AlunoNotasTela.fxml"));
            AnchorPane rootLayout = (AnchorPane) loader.load();
            scene = new Scene(rootLayout);
        } catch (IOException e) {
            System.out.println("oi");
            e.printStackTrace();
        }
        return scene;
    }
    
    private Scene getTelaEntrarTurma() {

        Scene scene = null;
        try {
            // Loader
            FXMLLoader loader = new FXMLLoader(getClass().getResource("AlunoTurmaTela.fxml"));
            AnchorPane rootLayout = (AnchorPane) loader.load();
            scene = new Scene(rootLayout);
        } catch (IOException e) {
            System.out.println("oi");
            e.printStackTrace();
        }
        return scene;
    }
    
    private Scene getTelaEntrarCurso() {

        Scene scene = null;
        try {
            // Loader
            FXMLLoader loader = new FXMLLoader(getClass().getResource("AlunoCursoTela.fxml"));
            AnchorPane rootLayout = (AnchorPane) loader.load();
            scene = new Scene(rootLayout);
        } catch (IOException e) {
            System.out.println("oi");
            e.printStackTrace();
        }
        return scene;
    }
    
}
