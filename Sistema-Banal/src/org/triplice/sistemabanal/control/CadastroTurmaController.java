/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.triplice.sistemabanal.control;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import org.triplice.sistemabanal.model.Curso;

/**
 *
 * @author pietro
 */
public class CadastroTurmaController extends Controller {
    
    @FXML
    TextField nome;
    
    @FXML
    ChoiceBox<Curso> cursos;
    
    @FXML
    ObservableList<Curso> cursoObs;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        cursoObs.addAll(Curso.loadAll());
        
        cursos.setItems(cursoObs);
    }
}
