/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.triplice.sistemabanal.control;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import org.triplice.sistemabanal.model.TAE;

/**
 *
 * @author User
 */
public class TAEController extends Controller{
    
    TAE tae;
    
    @FXML
    Label TAENome;
    @FXML
    Label TAEEmail;
    @FXML
    Label TAEEndereco;
    @FXML
    Label TAEFoto;
    @FXML
    Label TAEUsuario;
    @FXML
    Label TAESetor;
    @FXML
    Label TAESalario;

    public TAE getTae() {
        return tae;
    }

    public void setTae(TAE tae) {
        
        this.tae = tae;
    
        TAENome.setText(tae.getLogin());
        TAEEmail.setText(tae.getEmail());
        TAEEndereco.setText(tae.getEndereco());
        // TAEFoto.setText(tae.getFoto());
        TAEUsuario.setText(tae.getLogin());
        TAESetor.setText(tae.getSetor());
        TAESalario.setText(Double.toString(tae.getSalario()));
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
       
    }
    
    @FXML
    public void criarTurma() {
        this.stage.setScene(getTelaCadastroTurma());
    }
    
    @FXML
    public void criarCurso() {
        this.stage.setScene(getTelaCadastroCurso());
    }
    
    @FXML
    public void criarAluno() {
        this.stage.setScene(getTelaCadastroAluno());
    }
    
    @FXML
    public void criarDisciplina()  {
        this.stage.setScene(getTelaCadastroDisciplina());
    }
    
    @FXML
    public void criarTae() {
        this.stage.setScene(getTelaCadastroTae());
    }
    
    @FXML
    public void criarProf() {
        this.stage.setScene(getTelaCadastroProf());
    }
    
    @FXML
    public void criarAlu() {
        this.stage.setScene(getTelaCadastroAlu());
    }
    
    private Scene getTelaCadastroTurma() {

        Scene scene = null;
        try {
            // Loader
            FXMLLoader loader = new FXMLLoader(getClass().getResource("CadastroTurmaTela.fxml"));
            AnchorPane rootLayout = (AnchorPane) loader.load();
            CadastroTurmaController tc = loader.<CadastroTurmaController>getController();
            scene = new Scene(rootLayout);
        } catch (IOException e) {
            System.out.println("oi");
            e.printStackTrace();
        }
        return scene;
    }
    
    private Scene getTelaCadastroCurso() {

        Scene scene = null;
        try {
            // Loader
            FXMLLoader loader = new FXMLLoader(getClass().getResource("CadastroCursoTela.fxml"));
            AnchorPane rootLayout = (AnchorPane) loader.load();
            scene = new Scene(rootLayout);
        } catch (IOException e) {
            System.out.println("oi");
            e.printStackTrace();
        }
        return scene;
    }
    
    private Scene getTelaCadastroAluno() {

        Scene scene = null;
        try {
            // Loader
            FXMLLoader loader = new FXMLLoader(getClass().getResource("CadastroAlunoTela.fxml"));
            AnchorPane rootLayout = (AnchorPane) loader.load();
            scene = new Scene(rootLayout);
        } catch (IOException e) {
            System.out.println("oi");
            e.printStackTrace();
        }
        return scene;
    }
    
    private Scene getTelaCadastroDisciplina() {
    
        Scene scene = null;
        try {
            // Loader
            FXMLLoader loader = new FXMLLoader(getClass().getResource("CadastroDisciplina.fxml"));
            AnchorPane rootLayout = (AnchorPane) loader.load();
            scene = new Scene(rootLayout);
        } catch (IOException e) {
            System.out.println("oi");
            e.printStackTrace();
        }
        return scene;
    }
    
    private Scene getTelaCadastroTae() {
    
        Scene scene = null;
        try {
            // Loader
            FXMLLoader loader = new FXMLLoader(getClass().getResource("CadastroTAETela.fxml"));
            AnchorPane rootLayout = (AnchorPane) loader.load();
            scene = new Scene(rootLayout);
        } catch (IOException e) {
            System.out.println("oi");
            e.printStackTrace();
        }
        return scene;
    }
    
    private Scene getTelaCadastroProf() {
    
        Scene scene = null;
        try {
            // Loader
            FXMLLoader loader = new FXMLLoader(getClass().getResource("CadastroProfTela.fxml"));
            AnchorPane rootLayout = (AnchorPane) loader.load();
            scene = new Scene(rootLayout);
        } catch (IOException e) {
            System.out.println("oi");
            e.printStackTrace();
        }
        return scene;
    }
    
    private Scene getTelaCadastroAlu() {
    
        Scene scene = null;
        try {
            // Loader
            FXMLLoader loader = new FXMLLoader(getClass().getResource("CadastroAlunoTela.fxml"));
            AnchorPane rootLayout = (AnchorPane) loader.load();
            scene = new Scene(rootLayout);
        } catch (IOException e) {
            System.out.println("oi");
            e.printStackTrace();
        }
        return scene;
    }
}
