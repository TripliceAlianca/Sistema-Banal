/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.triplice.sistemabanal.control;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import org.triplice.sistemabanal.model.TAE;

/**
 *
 * @author User
 */
public class CadTAEController extends Controller {
    
    @FXML 
    TextField tfNome;
    @FXML
    TextField tfEmail;
    @FXML
    TextField tfEndereco;
    @FXML
    TextField tfFoto;
    @FXML
    TextField tfSenha;
    @FXML
    TextField tfUsuario;
    @FXML
    TextField tfSetor;
    @FXML
    TextField tfSalario;
    
    @FXML
    Button btCadastrar;
    
    @FXML
    public void cadastrar(){
        TAE t = new TAE();
        t.setNome(tfNome.getText());
        t.setEmail(tfEmail.getText());
        t.setEndereco(tfEndereco.getText());
        t.setFoto(tfFoto.getText());
        t.setLogin(tfUsuario.getText());
        t.setSenha(tfSenha.getText());
        t.setSetor(tfSetor.getText());
        t.setSalario(Double.parseDouble(tfSalario.getText()));        
        
        t.insert();
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {   
       
    }
    
}
