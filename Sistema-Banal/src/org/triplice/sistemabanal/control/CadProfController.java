/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.triplice.sistemabanal.control;

import java.net.URL;
import java.time.ZoneId;
import static java.time.temporal.TemporalQueries.localDate;
import java.util.Calendar;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import org.triplice.sistemabanal.model.Professor;

/**
 *
 * @author User
 */
public class CadProfController extends Controller{
    
    @FXML 
    TextField tfNome;
    @FXML
    TextField tfEmail;
    @FXML
    TextField tfEndereco;
    @FXML
    TextField tfFoto;
    @FXML
    TextField tfSenha;
    @FXML
    TextField tfLinkLattes;
    @FXML
    TextField tfUsuario;
    @FXML
    TextField tfArea;
    @FXML
    TextField tfSalario;
    
    @FXML
    DatePicker dpIngresso;
    
    @FXML
    Button btCadastrar;
    
    @FXML
    public void cadastrar(){
        Professor p = new Professor();
        p.setNome(tfNome.getText());
        p.setEmail(tfEmail.getText());
        p.setEndereco(tfEndereco.getText());
        p.setFoto(tfFoto.getText());
        p.setLogin(tfUsuario.getText());
        p.setSenha(tfSenha.getText());
        p.setArea(tfArea.getText());
        p.setLinkLattes(tfLinkLattes.getText());
        p.setSalario(Double.parseDouble(tfSalario.getText()));
        
        Date date = Date.from(dpIngresso.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant());
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        p.setDataIngresso(cal);
        
        p.insert();
                
    }
    
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {   
       
    }
}
