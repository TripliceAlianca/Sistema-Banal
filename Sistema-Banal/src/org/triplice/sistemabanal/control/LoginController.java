/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.triplice.sistemabanal.control;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import org.bigolin.Conexao;
import org.triplice.sistemabanal.model.Aluno;
import org.triplice.sistemabanal.model.Professor;
import org.triplice.sistemabanal.model.TAE;

/**
 *
 * @author Aluno
 */
public class LoginController extends Controller{
    
    @FXML
    TextField tfLogin;
    @FXML
    TextField tfSenha;
    @FXML
    Button bLogin;
    
    private void digameQuemEs(int id){
        
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps;
        
        try{
            ResultSet rs;
            ps = dbConnection.prepareStatement("select id from aluno WHERE id = ?");
            ps.setInt(1, id);
            rs = ps.executeQuery();
            if(rs.next()){
                Aluno a = new Aluno();
                a.load(id);
                this.stage.setScene(getAlunoTela(a));
                return;
            }
            ps = dbConnection.prepareStatement("select id from tae WHERE id = ?");
            ps.setInt(1, id);
            rs = ps.executeQuery();
            if(rs.next()){
                TAE t = new TAE();
                t.load(id);
                this.stage.setScene(getTAETela(t));
                return;                
            }
            ps = dbConnection.prepareStatement("select id from professor WHERE id = ?");
            ps.setInt(1, id);
            rs = ps.executeQuery();
            if(rs.next()){
                Professor p = new Professor();
                p.load(id);
                this.stage.setScene(getProfTela(p));
                return;
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
        
    }
    
    private Scene getAlunoTela(Aluno a) {

        Scene scene = null;
        try {
            // Loader
            FXMLLoader loader = new FXMLLoader(getClass().getResource("AlunoTela.fxml"));
            AnchorPane rootLayout = (AnchorPane) loader.load();
            AlunoController ac = loader.<AlunoController>getController();
            ac.setAluno(a);
            ac.setStage(stage);
            scene = new Scene(rootLayout);
        } catch (IOException e) {
            System.out.println("oi");
            e.printStackTrace();
        }
        return scene;
    }
    
    private Scene getProfTela(Professor prof) {

        Scene scene = null;
        try {
            // Loader
            FXMLLoader loader = new FXMLLoader(getClass().getResource("ProfTela.fxml"));
            AnchorPane rootLayout = (AnchorPane) loader.load();
            ProfController pc = loader.<ProfController>getController();
            pc.setProf(prof);
            pc.setStage(stage);
            scene = new Scene(rootLayout);
        } catch (IOException e) {
            System.out.println("oi");
            e.printStackTrace();
        }
        
        return scene;
    }
    
    private Scene getTAETela(TAE tae) {

        Scene scene = null;
        try {
            // Loader
            FXMLLoader loader = new FXMLLoader(getClass().getResource("TAETela.fxml"));
            AnchorPane rootLayout = (AnchorPane) loader.load();
            TAEController tc = loader.<TAEController>getController();
            tc.setTae(tae);
            tc.setStage(stage);
            scene = new Scene(rootLayout);
        } catch (IOException e) {
            System.out.println("oi");
            e.printStackTrace();
        }
        return scene;
    }
    
    public void validaLogin(){
        String selectSQL = "select id,login,senha,tentativas from usuario WHERE login = ?";
        String login, senha;
        login = tfLogin.getText();
        senha = tfSenha.getText();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps;
        
        try{
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setString(1, login);
            
            ResultSet rs = ps.executeQuery();
            
            if(rs.next()){
                if(rs.getString("login").equals(login)){
                    if(rs.getInt("tentativas") >= 3){
                        System.out.println("Tentativas maximas atingidas");
                        return;
                    }
                    if(rs.getString("senha").equals(senha) ){
                        zeraTentativas(rs.getInt("id"));
                        
                        digameQuemEs(rs.getInt("id"));
                        
                        return;
                    }
                    else {
                        incrementaLoginInvalido(rs.getInt("id"));
                        System.out.println("Senha invalida, tente novamente"); 
                        return;
                    }  
                }              
            }
            System.out.println("Usuario nao encontrado, tente novamente");
            return;
            
        }catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
    }
    
    private void incrementaLoginInvalido(int idUsuario){
        String updateSQL = "update usuario set tentativas = ? where id = ?";
        String selectSQL = "select tentativas from usuario where id = ?";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps;
        int tentativas = 0;
        try{
            //Pega as tentativas atuais
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, idUsuario);
            ResultSet rs = ps.executeQuery();
            
            if(rs.next()){
                tentativas = rs.getInt("tentativas");
            }
            
            
            ps = dbConnection.prepareStatement(updateSQL);
            ps.setInt(2, idUsuario);
            ps.setInt(1, tentativas+1);
            ps.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
    
    private void zeraTentativas(int idUsuario){
        String updateSQL = "update usuario set tentativas = ? where id = ?";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps;
        try{            
            ps = dbConnection.prepareStatement(updateSQL);
            ps.setInt(2, idUsuario);
            ps.setInt(1, 0);
            ps.executeUpdate();
        }catch(SQLException e){
           e.printStackTrace();
        }
    }

    
   
    @Override
    public void initialize(URL location, ResourceBundle resources) {   
       
    }
}
