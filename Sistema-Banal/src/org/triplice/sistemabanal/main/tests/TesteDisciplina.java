/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.triplice.sistemabanal.main.tests;

import org.triplice.sistemabanal.model.Disciplina;

/**
 *
 * @author Aluno
 */
public class TesteDisciplina {
    public static void main(String[] args) {
       
        Disciplina d = new Disciplina();
        
      d.setCronograma("Dia 1: tal coisa. Dia 2: tal outra coisa.");
      d.setEmenta("Faz tal e tal coisa.");
      d.setNome("Programação");
      d.setObjetivos("Receber meu salario.");
      d.setReferencias("Livro tal de tal data em tal idioma.");
      
      d.insert();
              
       Disciplina x = new Disciplina();
        x.load(1);
        x.setNome("Prog 101");
        
        x.update();
    
        //d.delete();
        
    }
}
