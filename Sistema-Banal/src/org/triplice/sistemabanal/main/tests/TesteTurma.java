/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.triplice.sistemabanal.main.tests;

import java.util.ArrayList;
import java.util.Calendar;
import org.triplice.sistemabanal.model.Aluno;
import org.triplice.sistemabanal.model.Curso;
import org.triplice.sistemabanal.model.Disciplina;
import org.triplice.sistemabanal.model.Horario;
import org.triplice.sistemabanal.model.Professor;
import org.triplice.sistemabanal.model.Turma;
import org.triplice.sistemabanal.model.Usuario;

/**
 *
 * @author Aluno
 */
public class TesteTurma {
     public static void main(String[] args) {
    
        Turma t = new Turma();
        
        ArrayList<Disciplina> disc = new ArrayList<>();
        Disciplina di = new Disciplina();
        di.setCronograma("Letras, Adjetivos, Substantivos");
        di.setEmenta("Teste");
        di.setNome("Nome");
        di.setObjetivos("Objetivo");
        di.setReferencias("Referencias");
        disc.add(di);
        
        ArrayList<Horario> horarios = new ArrayList<>();
        Horario ho = new Horario();
        ho.setDisciplina(di);
        ho.setInicio(Calendar.getInstance());
        ho.setFim(Calendar.getInstance());
        horarios.add(ho);
        
        ArrayList<Professor> professores = new ArrayList<>();
        professores.add(new Professor());
        
        ArrayList<Horario> provas = new ArrayList<>();
        Horario h = new Horario();
        h.setDisciplina(di);
        h.setInicio(Calendar.getInstance());
        h.setFim(Calendar.getInstance());
        provas.add(h);
        
        t.setCurso(new Curso());
        t.setDisciplinas(disc);
        t.setHorario(horarios);
        t.setProfessores(professores);
        t.setProvas(provas);
        
        t.insert();
        
        Turma t2 = new Turma();
        t2.load(1);
        
        t2.update();      
        
        t.delete();
     }
    
}
