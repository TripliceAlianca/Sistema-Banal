/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.triplice.sistemabanal.main.tests;

import java.util.ArrayList;
import org.triplice.sistemabanal.model.Curso;
import org.triplice.sistemabanal.model.Disciplina;
import org.triplice.sistemabanal.model.Turma;

/**
 *
 * @author Aluno
 */
public class TesteCurso {
    public static void main(String[] args) {
    
        Curso c = new Curso();   
        
        ArrayList<Disciplina> d = new ArrayList<Disciplina>();
        Disciplina d1 = new Disciplina();
        d1.setNome("oloco");
        Disciplina d2 = new Disciplina();
        d2.setNome("ffas");
        d.add(d1);
        d.add(d2);
        
        ArrayList<Turma> t = new ArrayList<Turma>();
        Turma t1 = new Turma();
        t.add(t1);
        
        c.setDisciplinas(d);
        c.setDuracao(400);
        c.setModalidade("Integrado");
        c.setPpc("PPC do curso");
        c.setTurno("Manha");
        c.setTurma(t);
        

        c.insert();
        
        Curso x = new Curso();
        x.load(1);
        x.setTurno("nait");
        
        x.update();
    
        //c.delete();
        
        
    }
}
