/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.triplice.sistemabanal.main.tests;

import java.util.Calendar;
import org.triplice.sistemabanal.model.Professor;

/**
 *
 * @author Aluno
 */
public class TesteProfessor {
    public static void main(String[] args) {
    
        Professor p = new Professor();
        
        p.setLogin("prof");
        p.setSenha("1");
        p.setEmail("teste@exemplo.com");
        p.setEndereco("rua teste");
        p.setFoto("https://upload.wikimedia.org/wikipedia/commons/4/49/Distribui%C3%A7%C3%A3o_Sim%C3%A9trica.gif");

        Calendar c = Calendar.getInstance();
        c.set(Calendar.DAY_OF_MONTH, 22);
        c.set(Calendar.MONTH, 3);
        c.set(Calendar.YEAR, 2017);
        
        p.setArea("Programation");
        p.setDataIngresso(c);
        p.setSalario(0);
        p.setLinkLattes("auau.com");
        
         
        p.insert();
        
        /*Professor x = new Professor();
        x.load(1);
        x.setLogin("deu certo!");
        
        x.update();      
        
    
        p.delete(); */

     }
}
