/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.triplice.sistemabanal.main.tests;

import org.triplice.sistemabanal.model.Usuario;

/**
 *
 * @author pietro
 */
public class TesteUsuario {
    
    public static void main(String[] args) {
        
        Usuario u = new Usuario();
        
        u.setLogin("teste");
        u.setSenha("teste");
        u.setEmail("teste@exemplo.com");
        u.setEndereco("rua teste");
        u.setFoto("https://upload.wikimedia.org/wikipedia/commons/4/49/Distribui%C3%A7%C3%A3o_Sim%C3%A9trica.gif");
        
        u.insert();
        
        Usuario x = new Usuario();
        x.load(1);
        x.setLogin("deu certo!");
        
        x.update();
    
        //u.delete();
    }
}
