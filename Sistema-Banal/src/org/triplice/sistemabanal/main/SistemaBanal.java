/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.triplice.sistemabanal.main;

import java.io.IOException;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.triplice.sistemabanal.control.LoginController;

/**
 *
 * @author pietro
 */
public class SistemaBanal extends Application{

    
    private Stage primaryStage; //isso não havia em outras classes
    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;

        primaryStage.setTitle("Login");
        primaryStage.setScene(getRootLayout());
        primaryStage.show();
    }
    
     private Scene getRootLayout() {

        Scene scene = null;
        try {
            // Loader
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(LoginController.class.getResource("LoginTela.fxml"));
            AnchorPane rootLayout = (AnchorPane) loader.load();
            
            //observe essas duas linhas
            LoginController tc = loader.getController();
            tc.setStage(primaryStage);
            
            scene = new Scene(rootLayout);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return scene;
    }
     
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
